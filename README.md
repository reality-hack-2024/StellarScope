# StellarScope: Connecting with the Cosmos

**Description**:

StellarScope is an immersive, interactive VR astronomy lesson for beginners. Players learn about convex, flat, and concave lenses, and how telescopes use lenses to bend light and magnify images. Players can connect the individual components to assemble a telescope. By looking through this telescope, players can view and learn about the constellations.

StellarScope can be used on a Meta Quest 2 or Meta Quest 3 headset.

## How to Install and Run

**Open the terminal on your computer**.

Clone this repository:

`git clone https://codeberg.org/reality-hack-2024/TABLE_52.git`

Navigate to the project directory:

`cd TABLE_52`

Make sure you have Unity3D installed. Open the project in Unity and let it automatically resolve and install any dependencies.

Ensure that your VR headset is connected and properly set up.

Configure VR Settings:

In Unity, go to `Edit -> Project Settings -> Player`. Under the XR Settings tab, make sure the appropriate VR SDKs are selected based on your VR headset.

Build the Game:

Go to `File -> Build Settings` and select your target platform. Click on `Build and Run` to generate the executable file for your VR headset.

### How to Use

Once in the game, use controllers to interact with lenses and different parts of the telescope. When looking through the telescope, move your head to learn more about the constellations.

### Credits

StellarScope was created by Mollie Johnson, Carlos Mendez, Clara Ma, Xinyi (Luna) Xu, and Ziwei (Tainy) Xu for the MIT Reality Hack 2024.

## License

This project is freely available under a Creative Commons license.
